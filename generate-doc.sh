#!/bin/bash

_DIR="html"

if [[ -n "$FOLDER" ]]; then
    _DIR=$FOLDER
fi

rm -rf $_DIR

for file in $(find openapi -maxdepth 2 -type f -name "*.json"); do
    yarn generate-doc "${file}" -o $_DIR/"${file##*/}".html
done
